function init (type) {
  var id = 0
  return (name) => {
    id += 1
    return {
      id: id,
      type: type,
      name: name
    }
  }
}

const createUser = init('user')
const createBook = init('book')
const dave = createUser('Dave')
const annie = createUser('Annie')
const nob = createBook('Node Cookbook')

console.log(dave)
console.log(annie)
console.log(nob)

